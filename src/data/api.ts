import LCValidator from './validation'

const lcv = new LCValidator()

export interface VersionResponse {
  version: {
    major: number,
    minor: number,
    patch: number
  }
}

/* eslint-disable camelcase */
export interface StreamMetadata {
    audio: {
      bitrate: number
      channels: number
      codec: string
      sample_rate: number
    }
    video: {
      bitrate: number
      codec: string
      framerate: number
      height: number
      width: number
    }
  }

export interface StreamStatsResponse {
  app_name: string
  metadata: StreamMetadata
  start_time: string
}
/* eslint-enable camelcase */

export interface ActiveStreamsResponse {
  streams: string[]
}

export interface NamesResponse {
  primary: string,
  alts: string[]
}

export interface CommandResponse {
  admin: boolean,
  partner: boolean,
  usage: string,
  names: NamesResponse,
  category: string,
  desc: string,
  sfw: boolean
}

export interface ModuleResponse {
  name: string,
  commands: CommandResponse[]
}

/* eslint-disable camelcase */
export interface LeaderboardResponse {
  user: {
    name: string,
    discriminator: string,
    user_id: number,
    avatar: string
  },
  value: number,
  level: {
    curr: number,
    next_req: number,
    next_perc: number
  },
  title: {
    tier: number,
    name: string
  }
}
/* eslint-enable camelcase */

interface Filter {
  keys: string[],
  values: string[]
}

class LuciaCoreAPI {
  lcDomain = process.env.VUE_APP_LC_DOMAIN
  coreApi = process.env.VUE_APP_CORE_API_URL
  streamApi = `${process.env.VUE_APP_STREAM_API_URL}/api`
  widgetApi = 'https://discordapp.com/api/guilds/200751504175398912/widget.json'
  key: string
  filters: Filter[]

  constructor () {
    this.key = lcv.makeKey()
    this.filters = []
  }

  setFilter (filterKeys: string[], filterValues: string[]) {
    this.filters.push({ keys: filterKeys, values: filterValues })
  }

  get<T> (uri: string): Promise<T> {
    let reqUri = uri
    const filters: string[] = []
    if (this.filters.length > 0) {
      this.filters.forEach((fltr) => {
        let keyString: string = ''
        fltr.keys.forEach((fltrKey) => {
          keyString += `[${fltrKey}]`
        })
        filters.push(`filter${keyString}=${fltr.values.join(',')}`)
      })
      reqUri += '?' + filters.join('&')
    }
    this.filters = []

    let headers: Headers
    if (reqUri.includes(this.lcDomain)) {
      headers = new Headers({ 'Authentication': this.key })
    } else {
      headers = new Headers()
    }

    return new Promise((resolve, reject) => {
      fetch(reqUri, { headers })
        .then((response) =>
          response.json().then((json) => {
            if (response.status !== 200) reject(json)
            else resolve(json)
          })
        )
        .catch((reason) => reject(reason))
    })
  }

  getActiveStreams (): Promise<ActiveStreamsResponse> {
    return this.get(this.streamApi + '/active-streams')
  }

  getStream (name: string): Promise<StreamStatsResponse> {
    return this.get(this.streamApi + '/stream-stats/' + name)
  }
}

export default LuciaCoreAPI
