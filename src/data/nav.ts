import images from '@/data/images'

export interface Nav {
  left: { name: string, url: string }[]
  right: { name: string, url: string, icon: string }[]
}

export interface SubNav extends Nav {
  logo: string
}

export interface Navigation extends Nav {
  subnavs: {
    [index: string]: SubNav
  }
}

const navigation: Navigation = {
  left: [
    { name: 'Home', url: '/' },
    { name: 'Projects', url: '/projects' },
    { name: 'About', url: '/about' },
    { name: 'Donate', url: '/donate' }
  ],
  right: [
    { name: 'GitLab', url: 'https://gitlab.com/lu-ci', icon: 'gitlab' },
    { name: 'Discord', url: 'https://discord.gg/aEUCHwX', icon: 'discord' }
  ],
  subnavs: {
    sigma: {
      logo: images.projects.sigma.white,
      left: [
        { name: 'Home', url: '/sigma' },
        { name: 'About', url: '/sigma/about' },
        { name: 'Stats', url: '/sigma/stats' },
        { name: 'Commands', url: '/sigma/commands' },
        { name: 'Leaderboards', url: '/sigma/leaderboards' }
      ],
      right: [
        { name: 'Repository', url: 'https://gitlab.com/lu-ci/sigma/apex-sigma', icon: 'gitlab' }
      ]
    },
    javelin: {
      logo: images.projects.javelin.white,
      left: [
        { name: 'Home', url: '/javelin' },
        { name: 'Live', url: '/javelin/live' },
        { name: 'Setup', url: '/javelin/setup' }
      ],
      right: [
        { name: 'Crate', url: 'https://crates.io/crates/javelin', icon: 'boxes' },
        { name: 'Repository', url: 'https://gitlab.com/valeth/javelin', icon: 'gitlab' }
      ]
    }
  }
}

export default navigation
