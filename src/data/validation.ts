class LCValidator {
  makeKey (): string {
    const sides = []
    const splitters = []
    let currentSum = 0
    const validSplitters = [1, 3, 5, 7, 11, 13]
    while (sides.length < 3) {
      const digits = []
      const limit = sides.length === 1 ? 9 : 10
      while (digits.length < limit) {
        const randomDigit = Math.floor(Math.random() * 15)
        digits.push(randomDigit)
        currentSum = currentSum + randomDigit
      }
      sides.push(digits)
      if (sides.length < 3) {
        splitters.push(validSplitters[Math.floor(Math.random() * validSplitters.length)])
      }
    }
    const currentMod = currentSum % 10
    let finalDigit: number
    if (currentMod > 8) {
      finalDigit = 10 - (currentMod - 8)
    } else {
      finalDigit = 8 - currentMod
    }
    let endSum = 0
    const pieces = [sides[0], [splitters[0]], sides[1], [finalDigit], [splitters[1]], sides[2]]
    const converted = []
    let pieceIndex
    for (pieceIndex in pieces) {
      let pieceDigit
      for (pieceDigit in pieces[pieceIndex]) {
        const pieceValue = pieces[pieceIndex][pieceDigit]
        converted.push(pieceValue.toString(16))
        if (Number(pieceIndex) !== 1 && Number(pieceIndex) !== 4) {
          endSum = endSum + pieceValue
        }
      }
    }
    if (endSum % 10 !== 8) {
      console.error('Failed generating proper key, this one has a modulo of: ' + endSum % 10)
    }
    return 'lc-' + converted.join('')
  }
}

export default LCValidator
