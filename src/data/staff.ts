export interface StaffMember {
  name: string,
  gl: string,
  gh: string,
  house: string | null
}

interface Staff {
  [user: string]: StaffMember
}

const staff: Staff = {
  alex: {
    name: 'Alex',
    gl: 'AXAz0r',
    gh: 'AXAz0r',
    house: 'Brilliance'
  },
  awakening: {
    name: 'Awakening',
    gl: 'awaken1ng',
    gh: 'awaken1ng',
    house: null
  },
  shifty: {
    name: 'Shifty',
    gl: 'Shifty',
    gh: 'Shifty6',
    house: 'Bravery'
  },
  valeth: {
    name: 'Valeth',
    gl: 'valeth',
    gh: 'valeth',
    house: 'Balance'
  }
}

export default staff
