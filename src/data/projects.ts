import staff, { StaffMember } from '@/data/staff'
import images from '@/data/images'

export interface Project {
  name: string,
  tagline: string,
  image: string,
  url: string,
  color: string,
  dev: StaffMember,
  external: boolean
}

const projects: Project[] = [
  {
    name: 'Apex Sigma',
    tagline: 'Made to bring knowledge to your Discord server.',
    image: images.projects.sigma.full,
    url: '/sigma',
    color: '#1B6F5F',
    dev: staff.alex,
    external: false
  },
  {
    name: 'Javelin',
    tagline: 'RTMP streaming server with the power of a torpedo.',
    image: images.projects.javelin.full,
    url: '/javelin',
    color: '#933E77',
    dev: staff.valeth,
    external: false
  },
  {
    name: 'Kyanite',
    tagline: 'The modular, expandable, gallery collector.',
    image: images.projects.kyanite.full,
    url: 'https://gitlab.com/lu-ci/kyanite',
    color: '#102BAB',
    dev: staff.alex,
    external: true
  },
  {
    name: 'BetterDiscord LINE Stickers',
    tagline: 'The moe BetterDiscord plugin with style.',
    image: images.core.lcWhiteCircleGlitch,
    url: 'https://github.com/awaken1ng/bd-linestickers',
    color: '#00B84F',
    dev: staff.awakening,
    external: true
  },
  {
    name: 'WaniKani Breeze Dark',
    tagline: 'Almighty Crabigator loves the dark.',
    image: images.projects.wkBreeze.full,
    url: 'https://gitlab.com/valeth/wanikani-breeze-dark',
    color: '#3490CF',
    dev: staff.valeth,
    external: true
  },
  {
    name: 'Snoo',
    tagline: 'Basic Reddit data processing for Rust.',
    image: images.projects.snoo.full,
    url: 'https://gitlab.com/lu-ci/snoo',
    color: '#F67400',
    dev: staff.alex,
    external: true
  }
]

export default projects
