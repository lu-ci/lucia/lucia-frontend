module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    'plugin:vue/essential',
    '@vue/standard',
    '@vue/typescript'
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'prefer-const': 'error'
  },
  overrides: [
    {
      files: ['src/types/**/*.d.ts'],
      rules: {
        'no-useless-constructor': 'off'
      }
    }
  ]
}
